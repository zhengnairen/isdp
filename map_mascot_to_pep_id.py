from collections import defaultdict
"""
with open("idpeptides.txt", "r") as f_in:
    with open("mascot_peptides_ids.txt", "w") as f_out:
        for line in f_in:
            peptide = line.strip()
            if peptide in peptides:
                f_out.write("%d\n" % peptides[peptide])
"""
files = defaultdict(list)
with open("expname_taxonomy_peptide.csv", "r") as f_in:
    for line in f_in:
        t = line.split(",")
        expname = t[0] + ".txt"
        taxonomy = t[1]
        peptide = t[2].strip()
        if taxonomy == "protozoa":
            #if peptide in peptides:
                #files[expname].append(peptides[peptide])
                files[expname].append(peptide)
for filename in files:
    with open(filename, "w") as f:
        for peptide in files[filename]:
            f.write("%s\n" % peptide)
