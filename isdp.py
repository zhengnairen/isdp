import os
import re
import sqlite3
import time

from Bio import Entrez
import pymysql.cursors

Entrez.email = os.getenv("ISDP_EMAIL", "A.N.Other@example.com")

connection = pymysql.connect(host=os.environ["ISDP_DBHOST"], user=os.environ["ISDP_DBUSER"], password=os.environ["ISDP_DBPASSWORD"], db=os.environ["ISDP_DBNAME"], charset="utf8mb4", cursorclass=pymysql.cursors.DictCursor)
cursor = connection.cursor()
sqls_init_db = [
    "CREATE TABLE IF NOT EXISTS species (id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, ncbi_txid BIGINT UNSIGNED UNIQUE, ncbi_scientific_name TEXT, UNIQUE (ncbi_scientific_name(255)))",
    "CREATE TABLE IF NOT EXISTS gene (id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, ncbi_gene_id BIGINT UNSIGNED UNIQUE, ncbi_gene_symbol TEXT)",
    "CREATE TABLE IF NOT EXISTS protein (id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, ncbi_refseq_protein_gi BIGINT UNSIGNED UNIQUE, ncbi_refseq_protein_accession TEXT, UNIQUE (ncbi_refseq_protein_accession(15)))",
    "CREATE TABLE IF NOT EXISTS expressed_in (id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, species_id BIGINT UNSIGNED NOT NULL, protein_id BIGINT UNSIGNED NOT NULL, UNIQUE (species_id, protein_id))",
    "CREATE TABLE IF NOT EXISTS coding_for(id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, gene_id BIGINT UNSIGNED NOT NULL, protein_id BIGINT UNSIGNED NOT NULL, UNIQUE (gene_id, protein_id))",
    "CREATE TABLE IF NOT EXISTS peptide (id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, sequence TEXT, UNIQUE (sequence(255)))",
    "CREATE TABLE IF NOT EXISTS digested_to (id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, protein_id BIGINT UNSIGNED NOT NULL, peptide_id BIGINT UNSIGNED NOT NULL, UNIQUE (protein_id, peptide_id))"
]
for sql in sqls_init_db:
    cursor.execute(sql)


sconn = sqlite3.connect("bim.db")
sc = sconn.cursor()

ncbi_refseq_protein_faa_path = "/home/unixer/projects/idconvert/v2/protozoa_20181126_protein.faa"
with open(ncbi_refseq_protein_faa_path, "r") as ncbi_refseq_protein_faa:
    count = 0
    for line in ncbi_refseq_protein_faa:
        if line.startswith(">"):
            count += 1
            if count % 1000 == 0:
                connection.commit()
                print("%d processed." % count)
            ncbi_refseq_protein_accession, line = line[1:].split(" ", 1)
            #ncbi_scientific_name = line.split("[")[1][:-2]
            m = re.search(".*\[(.*)\].*", line)
            if m:
                ncbi_scientific_name = m.group(1)
            else:
                print("No species name: %s %s" % (ncbi_refseq_protein_accession, line))
                print("Fetching from NCBI")
                with Entrez.efetch(db="protein", id=ncbi_refseq_protein_accession, rettype="gb", retmode="text") as handle:
                    m = re.search("ORGANISM\s*(.*?)\n", handle.read())
                if m:
                    ncbi_scientific_name = m.group(1)
                else:
                    print("No species name: %s %s" % (ncbi_refseq_protein_accession, line))
            if cursor.execute("SELECT id FROM species WHERE ncbi_scientific_name = %s", ncbi_scientific_name):
                species_id = cursor.fetchone()["id"]
            else:
                cursor.execute("INSERT INTO species (ncbi_scientific_name) VALUES (%s)", ncbi_scientific_name)
                #print("INSERT INTO species (ncbi_scientific_name) VALUES (%s)" % ncbi_scientific_name)
                #time.sleep(0.1)
                species_id = cursor.lastrowid
            if not cursor.execute("SELECT id FROM protein WHERE ncbi_refseq_protein_accession = %s", ncbi_refseq_protein_accession):
                sc.execute("SELECT * FROM proteins WHERE refseq_protein_version = ?", (ncbi_refseq_protein_accession, ))
                sr = sc.fetchone()
                if sr and not sr[6]:
                    ncbi_refseq_protein_gi = sr[0]
                    ncbi_gene_id = sr[3]
                    ncbi_gene_symbol = sr[4]
                    cursor.execute("INSERT INTO protein (ncbi_refseq_protein_gi, ncbi_refseq_protein_accession) VALUES(%s, %s)", (ncbi_refseq_protein_gi, ncbi_refseq_protein_accession))
                    #print("INSERT INTO protein (ncbi_refseq_protein_gi, ncbi_refseq_protein_accession) VALUES(%s, %s)" % (ncbi_refseq_protein_gi, ncbi_refseq_protein_accession))
                    #time.sleep(0.1)
                    protein_id = cursor.lastrowid
                    if cursor.execute("SELECT id FROM gene WHERE ncbi_gene_id = %s", ncbi_gene_id):
                        gene_id = cursor.fetchone()["id"]
                    else:
                        cursor.execute("INSERT INTO gene (ncbi_gene_id, ncbi_gene_symbol) VALUES (%s, %s)", (ncbi_gene_id, ncbi_gene_symbol))
                        #print("INSERT INTO gene (ncbi_gene_id, ncbi_gene_symbol) VALUES (%s, %s)" % (ncbi_gene_id, ncbi_gene_symbol))
                        #time.sleep(0.1)
                        gene_id = cursor.lastrowid
                    cursor.execute("INSERT INTO coding_for (gene_id, protein_id) VALUES (%s, %s)", (gene_id, protein_id))
                else:
                    cursor.execute("INSERT INTO protein (ncbi_refseq_protein_accession) VALUES(%s)", ncbi_refseq_protein_accession)
                    protein_id = cursor.lastrowid
                cursor.execute("INSERT INTO expressed_in (species_id, protein_id) VALUES(%s, %s)", (species_id, protein_id))
cursor.close()
connection.commit()
connection.close()
