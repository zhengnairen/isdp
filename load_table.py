from collections import defaultdict
ncbi_refseq_species_digested = defaultdict(list)
with open("species_peptide.csv", "r") as f:
    for i, line in enumerate(f):
        print("%d species loaded." % (i + 1))
        t = line.split(",")
        ncbi_refseq_species_digested[t[0]] = t[1].split(";")
