with open("idpeptides.txt", "r") as f:
    peptides = f.read().split()
with open("idpeptides_unique.csv", "w") as f:
    for peptide in peptides:
        for ncbi_refseq_species_key in ncbi_refseq_species_unique:
            if peptide in ncbi_refseq_species_unique[ncbi_refseq_species_key]:
                print("%s\t%s" % (ncbi_refseq_species_key, peptide))
                f.write("%s,%s\n" % (ncbi_refseq_species_key, peptide))
