import os
import re
import time
import pyteomics
import pyteomics.parser
import itertools
from Bio import Entrez
from collections import defaultdict
import sys


Entrez.email = os.getenv("ISDP_EMAIL", "A.N.Other@example.com")

ncbi_refseq_protein_faa_paths = ["/home/unixer/archaea_20181126_protein.faa", "/home/unixer/protozoa_20181126_protein.faa"]
#ncbi_refseq_protein_faa_paths = ["/home/unixer/archaea_20181126_protein.faa"]
ncbi_refseq_protein_faa_paths = ["/home/unixer/protozoa_20181126_protein.faa"]
#ncbi_refseq_protein_faa_paths = ["/home/unixer/protozoa_20181126_protein_sample.faa"]

pattern = ">?([^ ]*)[^\n]*\[(.*)\]\n([A-Z\n]*)$"

ncbi_refseq_species = defaultdict(list)
ncbi_refseq_species_digested = defaultdict(list)
ncbi_refseq_species_unique = defaultdict(list)
for ncbi_refseq_protein_faa_path in ncbi_refseq_protein_faa_paths:
    with open(ncbi_refseq_protein_faa_path, "r") as ncbi_refseq_protein_faa:
        fasta = ncbi_refseq_protein_faa.read()
    for i, protein in enumerate(fasta.split("\n>")):
        m = re.search(pattern, protein)
        if m:
            ncbi_refseq_protein_accession = m.group(1)
            ncbi_refseq_species_scientific_name = m.group(2)
            ncbi_refseq_protein_sequence = m.group(3).replace("\n", "")
            ncbi_refseq_species[ncbi_refseq_species_scientific_name].append(ncbi_refseq_protein_sequence)
        else:
            m = re.search(">?([^ ]*)[^\n]*\n([A-Z\n]*)$", protein)
            ncbi_refseq_protein_accession = m.group(1)
            ncbi_refseq_protein_sequence = m.group(2).replace("\n", "")
            with Entrez.efetch(db="protein", id=ncbi_refseq_protein_accession, rettype="gb", retmode="text") as handle:
                m = re.search("ORGANISM\s*(.*?)\n", handle.read())
            if m:
                ncbi_refseq_species_scientific_name = m.group(1)
                print("%s: %s, %s" % (ncbi_refseq_protein_accession, ncbi_refseq_species_scientific_name, ncbi_refseq_protein_sequence))
                ncbi_refseq_species[ncbi_refseq_species_scientific_name].append(ncbi_refseq_protein_sequence)
            else:
                print("No species name for %s." % ncbi_refseq_protein_accession)
        if i % 10000 == 0:
            print("%d proteins readed from fasta" % i)
print("%d proteins readed from fasta" % i)
for i, ncbi_refseq_species_key in enumerate(ncbi_refseq_species):
    ncbi_refseq_species_digested[ncbi_refseq_species_key] = set(list(itertools.chain.from_iterable([pyteomics.parser.cleave(sequence=ncbi_refseq_protein_sequence, rule=pyteomics.parser.expasy_rules["trypsin"], missed_cleavages=2) for ncbi_refseq_protein_sequence in ncbi_refseq_species[ncbi_refseq_species_key]])))
    print("Digested proteins in %d species." % i)
for i, ncbi_refseq_species_key_i in enumerate(ncbi_refseq_species_digested):
    ncbi_refseq_species_unique[ncbi_refseq_species_key_i] = ncbi_refseq_species_digested[ncbi_refseq_species_key_i]
    for j, ncbi_refseq_species_key_j in enumerate(ncbi_refseq_species_digested):
        if j != i:
            ncbi_refseq_species_unique[ncbi_refseq_species_key_i] = ncbi_refseq_species_unique[ncbi_refseq_species_key_i] - ncbi_refseq_species_digested[ncbi_refseq_species_key_j]
    print("Processed %d species to find unique peptides." % i)
with open("unique_peptides_201812031532.csv", "w") as f:
    for ncbi_refseq_species_key in ncbi_refseq_species_unique:
        peptide_ids = []
        for peptide in ncbi_refseq_species_unique[ncbi_refseq_species_key]:
            peptide_ids.append(str(peptides[peptide]))
        f.write("%s\t%s\n" % (ncbi_refseq_species_key, "\t".join(peptide_ids)))
