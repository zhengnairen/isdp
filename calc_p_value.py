import glob
from collections import defaultdict

# map_mascot_to_pep_id.py alreadly make sure peptides in these files are in peptides_id dict
mascot_result_paths = ["Exp031253_unique.txt", "Exp031254_unique.txt", "Exp033347_unique.txt", "Exp033348_unique.txt", "Exp033349_unique.txt"]
mascot_result_paths = ["Exp031253_unique.txt"]

ncbi_refseq_species_unique_peptide_id = defaultdict(list)
for key in ncbi_refseq_species_unique:
    for peptide in ncbi_refseq_species_unique[key]:
        ncbi_refseq_species_unique_peptide_id[key].append(peptides[peptide])

# a count of identified
# b count of not identified
# c count of identified in other species
# d count of not identified in other species
sum_identified = 0
sum_mascot = 0
ncbi_refseq_species_abcd = defaultdict(defaultdict)
for mascot_result_path in mascot_result_paths:
    with open(mascot_result_path, "r") as f:
        for species in ncbi_refseq_species_unique_peptide_id:
            for line in f:
                peptide_id = int(line.strip())
                if peptide_id in ncbi_refseq_species_unique_peptide_id[species]:
                    ncbi_refseq_species_abcd[species]["a"] += 1
                    sum_identified += 1
                else:
                    ncbi_refseq_species_abcd[species]["b"] += 1
