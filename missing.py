import pymysql, os, re

#connection = pymysql.connect(host=os.environ["ISDP_DBHOST"], user=os.environ["ISDP_DBUSER"], password=os.environ["ISDP_DBPASSWORD"], db=os.environ["ISDP_DBNAME"], charset="utf8mb4", cursorclass=pymysql.cursors.DictCursor)
#cursor = connection.cursor()

#cursor.execute("SELECT ncbi_refseq_protein_accession FROM protein")
#ncbi_refseq_protein_accessions = cursor.fetchall()

ncbi_refseq_protein_faa_path = "/home/unixer/projects/idconvert/v2/protozoa_20181126_protein.faa"
with open(ncbi_refseq_protein_faa_path, "r") as ncbi_refseq_protein_faa:
    fasta = ncbi_refseq_protein_faa.read()
acs0 = re.findall("(?:^|\n)>([^ ]*)", fasta)
