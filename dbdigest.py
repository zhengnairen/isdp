import re
import os
import pyteomics
import pyteomics.parser
import pymysql.cursors
import itertools
from multiprocessing.dummy import Pool as ThreadPool


"""
def digest(sequence):
    return pyteomics.parser.cleave(sequence, pyteomics.parser.expasy_rules["trypsin"], 2)


def digestParallel(peptides, threads=2):
    pool = ThreadPool(threads)
    results = set(list(itertools.chain.from_iterable(pool.map(digest, peptides))))
    pool.close()
    pool.join()
    return results
"""


connection = pymysql.connect(host=os.environ["ISDP_DBHOST"], user=os.environ["ISDP_DBUSER"], password=os.environ["ISDP_DBPASSWORD"], db=os.environ["ISDP_DBNAME"], charset="utf8mb4", cursorclass=pymysql.cursors.DictCursor)
cursor = connection.cursor()

print("Loading from database:")
cursor.execute("SELECT B.species_id, A.ncbi_refseq_protein_sequence FROM protein A INNER JOIN expressed_in B ON A.id = B.protein_id INNER JOIN species C ON B.species_id = C.id")
proteins = cursor.fetchall()
species = {}
print("Assigning proteins to species:")
for i, protein in enumerate(proteins):
    print("%d of %d proteins processed." % (i + 1, len(proteins)))
    species_id = protein["species_id"]
    ncbi_refseq_protein_sequence = protein["ncbi_refseq_protein_sequence"]
    if species_id not in species:
        species[species_id] = []
    species[species_id].append(ncbi_refseq_protein_sequence)

print("Digesting proteins in species:")
species_digested = {}
for i, key in enumerate(species):
    print("%d of %d species processed." % (i + 1, len(species)))
    print("%d proteins to be digested." % len(species[key]))
    species_digested[key] = set(list(itertools.chain.from_iterable([pyteomics.parser.cleave(sequence, pyteomics.parser.expasy_rules["trypsin"], 2) for sequence in species[key]])))

import multiprocessing
manager = multiprocessing.Manager()
species_unique = manager.dict()

def get_unique_peptides(species_digested, key, species_unique):
    species_unique[key] = species_digested[key]
    for i, key_i in enumerate(species_digested):
        print("Worker[%d]: %d species peptides removed." % (key, i))
        if key_i != key:
            species_unique[key] = species_unique[key] - species_digested[key_i]

print("Find unique peptides in species:")
p = {}
for key in species_digested:
    p[key] = multiprocessing.Process(target=get_unique_peptides, args=(species_digested, key, species_unique))
    p[key].start()
    while True:
        all_running = 0
        for key_p in p:
            if p[key_p].is_alive():
                all_running += 1
        if all_running < 8:
            break


"""
species_unique = {}
for i, key_i in enumerate(species_digested):
    print("%d of %d species processed." % (i + 1, len(species)))
    species_unique[key_i] = species_digested[key_i]
    for j, key_j in enumerate(species_digested):
        print("%d of %d species peptides removed." % (j + 1, len(species)))
        if key_j != key_i:
            species_unique[key_i] = species_unique[key_i] - species_digested[key_j]
"""

"""
    peptides = pyteomics.parser.cleave(sequence, pyteomics.parser.expasy_rules["trypsin"], 2)
    for peptide in peptides:
        count += 1
        if count % 1000 == 0:
            print("%dth protein: %d peptides processed." % (count_pr, count))
            connection.commit()
        if cursor.execute("SELECT id FROM peptide WHERE sequence = %s", peptide):
            peptide_id = cursor.fetchone()["id"]
        else:
            cursor.execute("INSERT INTO peptide (sequence) VALUES (%s)", peptide)
            peptide_id = cursor.lastrowid
        cursor.execute("INSERT INTO digested_to (protein_id, peptide_id) VALUES (%s, %s)", (protein_id, peptide_id))
"""
