"""
with open("species_peptide.csv", "w") as f:
    for i, ncbi_refseq_species_key in enumerate(ncbi_refseq_species_digested):
        if i % 32 == 0:
            print("%s,%s" % (ncbi_refseq_species_key, ";".join(ncbi_refseq_species_digested[ncbi_refseq_species_key])[:20]))
        f.write("%s,%s\n" % (ncbi_refseq_species_key, ";".join(ncbi_refseq_species_digested[ncbi_refseq_species_key])))
"""
"""
peptide_id = 0
peptides = {}
for ncbi_refseq_species_key in ncbi_refseq_species_digested:
    for peptide in ncbi_refseq_species_digested[ncbi_refseq_species_key]:
        if peptide not in peptides:
            peptide_id += 1
            peptides[peptide] = peptide_id
"""

"""
peptide_id = 0
peptides = {}
i = 0
with open("species_peptide.csv", "w") as f:
    for ncbi_refseq_species_key in ncbi_refseq_species_digested:
        i += 1
        if peptide_id % 128 == 0:
            print("%d species writed" % i)
            print("%d peptide id generated" % peptide_id)
        f.write("%s\t" % ncbi_refseq_species_key)
        peptides_ids = []
        for peptide in ncbi_refseq_species_digested[ncbi_refseq_species_key]:
            if peptide not in peptides:
                peptide_id += 1
                peptides[peptide] = peptide_id
            peptides_ids.append(str(peptides[peptide]))
        f.write("%s\n" % "\t".join(peptides_ids))
"""
"""
i = 0
with open("peptide_dict.csv", "w") as f:
    for peptide in peptides:
        i += 1
        if i > 0 and i % 1024 == 0:
            print("%d peptides saved." % i)
        f.write("%s,%d\n" % (peptide, peptides[peptide]))
"""
