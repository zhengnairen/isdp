import time
from collections import defaultdict
ncbi_refseq_species_with_unique_peptide_id = defaultdict(list)
"""
sum = 0
for key in ncbi_refseq_species_with_unique_peptide_id:
    print(key)
    sum += len(ncbi_refseq_species_with_unique_peptide_id([key]))
    print(ncbi_refseq_species_with_unique_peptide_id[key])
print("sum = %d" % sum)
"""
"""
with open("unique_peptides.csv", "r") as f:
    for i, line in enumerate(f):
        record = line.strip().split(",", 1)
        ncbi_refseq_species_with_unique_peptide_id[record[0]].append(peptides[record[1]])
        if (i + 1) % 5000 == 0:
            print("%d peptide loaded from file." % (i + 1))
print("%d peptide loaded from file." % (i + 1))
"""

"""
peptide_with_species_set = defaultdict(list)
for i, ncbi_refseq_species_key in enumerate(ncbi_refseq_species_with_peptide_id):
    print("%d species processed." % i)
    for peptide_id in ncbi_refseq_species_with_unique_peptide_id[ncbi_refseq_species_key]:
        peptide_with_species_set[peptide_id].append(ncbi_refseq_species_key)



ncbi_refseq_species_with_unique_peptide_id = defaultdict(list)
for i, key in enumerate(peptide_with_species_set):
    peptide_with_species_set[key] = set(peptide_with_species_set[key])
    if i % 1024 == 0:
        print("%d peptides processed." % i)
    if len(peptide_with_species_set[key]) == 1:
        ncbi_refseq_species_with_unique_peptide_id[peptide_with_species_set[key][0]].append(key)
"""

"""
for i, ncbi_refseq_species_key_i in enumerate(ncbi_refseq_species_with_peptide_id):
    ncbi_refseq_species_with_unique_peptide_id[ncbi_refseq_species_key_i] = ncbi_refseq_species_with_peptide_id[ncbi_refseq_species_key_i]
    for j, ncbi_refseq_species_key_j in enumerate(ncbi_refseq_species_with_peptide_id):
        if j != i:
            ncbi_refseq_species_with_unique_peptide_id[ncbi_refseq_species_key_i] = ncbi_refseq_species_with_unique_peptide_id[ncbi_refseq_species_key_i] - ncbi_refseq_species_with_peptide_id[ncbi_refseq_species_key_j]
    print("Processed %d species to find unique peptides." % i)
"""
