import os
import re
import time
import pyteomics
import pyteomics.parser
from Bio import Entrez
import multiprocessing
from ctypes import c_bool, c_int
from collections import defaultdict

manager = multiprocessing.Manager()
ncbi_refseq_species = manager.dict()
ncbi_refseq_species_digested = manager.dict()
ncbi_refseq_species_unique = manager.dict()
READING_FASTA = manager.Value(c_bool, True)
count = manager.Value(c_int, 0)

ncbi_refseq_species_local = defaultdict(list)
ncbi_refseq_species_digested_local = defaultdict(list)
ncbi_refseq_species_unique_local = defaultdict(list)

def digest(ncbi_refseq_species, ncbi_refseq_species_digested, READING_FASTA, count):
    ncbi_refseq_species_local = defaultdict(list)
    ncbi_refseq_species_digested_local = defaultdict(list)
    ncbi_refseq_species_local.update(ncbi_refseq_species)
    ncbi_refseq_species_digested_local.update(ncbi_refseq_species_digested)
    while READING_FASTA.value or DIGESTING_PROTEINS:
        DIGESTING_PROTEINS = READING_FASTA.value
        if not ncbi_refseq_species_local:
            print("sleeping")
            time.sleep(1)
        for ncbi_refseq_species_key in ncbi_refseq_species_local:
            print(ncbi_refseq_species_key)
            while ncbi_refseq_species_local[ncbi_refseq_species_key]:
                DIGESTING_PROTEINS = True
                ncbi_refseq_protein = ncbi_refseq_species_local.pop()
                ncbi_refseq_species_digested_local[ncbi_refseq_species_key].extend(pyteomics.parser.cleave(sequence=ncbi_refseq_protein, rule=pyteomics.parser.expasy_rules["trypsin"], missed_cleavages=2))
                count.set(count.value+1)
                print("%d proteins digested" % count.value)
                if count.value % 1024 == 0:
                    print("%d proteins digested" % count.value)
        ncbi_refseq_species.update(ncbi_refseq_species_local)
        ncbi_refseq_species_digested.update(ncbi_refseq_species_digested_local)


Entrez.email = os.getenv("ISDP_EMAIL", "A.N.Other@example.com")

#ncbi_refseq_protein_faa_paths = ["/home/unixer/archaea_20181126_protein.faa", "/home/unixer/protozoa_20181126_protein_sample.faa"]
ncbi_refseq_protein_faa_paths = ["/home/unixer/archaea_20181126_protein.faa", "/home/unixer/protozoa_20181126_protein.faa"]

pattern = ">?([^ ]*)[^\n]*\[(.*)\]\n([A-Z\n]*)$"
#pattern = ">?[^\n\[]*\[(.*)\]\n([A-Z\n]*)$"

p_digest = multiprocessing.Process(target=digest, args=(ncbi_refseq_species, ncbi_refseq_species_digested, READING_FASTA, count))
p_digest.start()
count_fuck = 0
for ncbi_refseq_protein_faa_path in ncbi_refseq_protein_faa_paths:
    with open(ncbi_refseq_protein_faa_path, "r") as ncbi_refseq_protein_faa:
        fasta = ncbi_refseq_protein_faa.read()
    for protein in fasta.split("\n>"):
        m = re.search(pattern, protein)
        if m:
            ncbi_refseq_protein_accession = m.group(1)
            ncbi_refseq_species_scientific_name = m.group(2)
            ncbi_refseq_protein_sequence = m.group(3).replace("\n", "")
            #print(ncbi_refseq_protein_sequence)
            #proteins.append((ncbi_refseq_protein_accession, ncbi_refseq_species_scientific_name, ncbi_refseq_protein_sequence))
            #if ncbi_refseq_species_scientific_name not in ncbi_refseq_species:
                #ncbi_refseq_species[ncbi_refseq_species_scientific_name] = []
            #ncbi_refseq_species[ncbi_refseq_species_scientific_name] += [ncbi_refseq_protein_sequence]
            ncbi_refseq_species_local[ncbi_refseq_species_scientific_name].append(ncbi_refseq_protein_sequence)
        else:
            continue
            m = re.search(">?([^ ]*)[^\n]*\n([A-Z\n]*)$", protein)
            ncbi_refseq_protein_accession = m.group(1)
            ncbi_refseq_protein_sequence = m.group(2).replace("\n", "")
            with Entrez.efetch(db="protein", id=ncbi_refseq_protein_accession, rettype="gb", retmode="text") as handle:
                m = re.search("ORGANISM\s*(.*?)\n", handle.read())
            if m:
                ncbi_refseq_species_scientific_name = m.group(1)
                print("%s: %s, %s" % (ncbi_refseq_protein_accession, ncbi_refseq_species_scientific_name, ncbi_refseq_protein_sequence))
                time.sleep(1)
                #if ncbi_refseq_species_scientific_name not in ncbi_refseq_species:
                    #ncbi_refseq_species[ncbi_refseq_species_scientific_name] = []
                #ncbi_refseq_species[ncbi_refseq_species_scientific_name] += [ncbi_refseq_protein_sequence]
                ncbi_refseq_species_local[ncbi_refseq_species_scientific_name].append(ncbi_refseq_protein_sequence)
            else:
                print("No species name for %s." % ncbi_refseq_protein_accession)
                time.sleep(5)
        count_fuck += 1
        if count_fuck % 1024 == 0:
            print("%d updated" % count_fuck)
            ncbi_refseq_species.update(ncbi_refseq_species_local)
            print(len(ncbi_refseq_species))
        if count_fuck % 10240 == 0:
            print("%d proteins readed from fasta" % count_fuck)
            ncbi_refseq_species.update(ncbi_refseq_species_local)
            break
#READING_FASTA.set(False)
p_digest.join()
