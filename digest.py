import re
import os
import pyteomics
import pyteomics.parser
import pymysql.cursors
connection = pymysql.connect(host=os.environ["ISDP_DBHOST"], user=os.environ["ISDP_DBUSER"], password=os.environ["ISDP_DBPASSWORD"], db=os.environ["ISDP_DBNAME"], charset="utf8mb4", cursorclass=pymysql.cursors.DictCursor)
cursor = connection.cursor()


ncbi_refseq_protein_faa_path = "/home/unixer/projects/idconvert/v2/protozoa_20181126_protein.faa"
with open(ncbi_refseq_protein_faa_path, "r") as ncbi_refseq_protein_faa:
    fasta = ncbi_refseq_protein_faa.read()
pattern = ">?([^ ]*)(?:.*?)\n([A-Z\n]*)$"

matches = [re.search(pattern, _) for _ in fasta.split("\n>")]
proteins = [(match.group(1), match.group(2).replace("\n", "")) for match in matches]
count = 0
count_pr = 0
for ncbi_refseq_protein_accession, sequence in proteins:
    count_pr += 1
    cursor.execute("SELECT id FROM protein WHERE ncbi_refseq_protein_accession = %s", ncbi_refseq_protein_accession)
    protein_id = cursor.fetchone()["id"]
    peptides = pyteomics.parser.cleave(sequence, pyteomics.parser.expasy_rules["trypsin"], 2)
    for peptide in peptides:
        count += 1
        if count % 1000 == 0:
            print("%dth protein: %d peptides processed." % (count_pr, count))
            connection.commit()
        if cursor.execute("SELECT id FROM peptide WHERE sequence = %s", peptide):
            peptide_id = cursor.fetchone()["id"]
        else:
            cursor.execute("INSERT INTO peptide (sequence) VALUES (%s)", peptide)
            peptide_id = cursor.lastrowid
        cursor.execute("INSERT INTO digested_to (protein_id, peptide_id) VALUES (%s, %s)", (protein_id, peptide_id))
