import datetime
import itertools
import os
import re
import sys
import pyteomics
import pyteomics.parser
import humanize
import sqlite3
from multiprocessing import Pool, Value
from functools import reduce

SQLITE_MAX_VARIABLE_NUMBER = 999

counter = None
#longest_peptide_length = None

def init_digest(args):
    global counter
    counter = args

"""
def init_measure(args):
    global longest_peptide_length
    longest_peptide_length = args
"""

def digest_worker(species):
    global counter
    species.digest()
    with counter.get_lock():
        counter.value += 1
    print("\r%d species digested." % counter.value, end="")
    return species

"""
def measure_worker(species):
    global longest_peptide_length
    length = len(max(species.peptides, key=len))
    with longest_peptide_length.get_lock():
        if longest_peptide_length.value < length:
            longest_peptide_length.value = length
"""

def reduce_worker(species):
    species_i = species[0]
    t = []
    for s in species:
        t.append(set(s.peptides))
    species_i.unique_peptides = reduce(lambda x,y: x-y, t)
    print(".", end="", flush=True)
    return species_i

class Fasta():
    def __init__(self, path):
        p = ">([^ ]*)([^\n]*)\n([A-Z\n]*)"
        if re.search(p, "".join(itertools.islice(open(path, "r"), 10))):
            self.__path = path
            self.__len = 0
            self.__proteins = []
            self.__species = []
            self.__loaded = False
            self.__digested = False
            self.__reduced = False
            #self.__longest_peptide_length = 0
        else:
            raise ValueError("FASTA file corrupted.")
    def __str__(self):
        return self.__path
    def __repr__(self):
        return "<Fasta path: %s>" % self.__path
    def __len__(self):
        if self.__len == 0:
            with open(self.__path, "r") as f:
                for c in iter(lambda: f.read(1), ""):
                    if ">" == c:
                        self.__len += 1
                    else:
                        next(f)
        return self.__len
    def load(self):
        if not self.__loaded:
            species_dict = {}
            with open(self.__path, "r") as f:
                for c in iter(lambda: f.read(1), ""):
                    if c == ">":
                        if f.tell() > 1:
                            if scientific_name:
                                if scientific_name in species_dict:
                                    species = species_dict[scientific_name]
                                else:
                                    species = Species(scientific_name)
                                    self.__species.append(species)
                                    species_dict[scientific_name] = species
                            else:
                                species = None
                            """
                            species = next((x for x in self.__species if x.scientific_name == scientific_name), None)
                            if not species and scientific_name:
                                species = Species(scientific_name)
                                self.__species.append(species)
                                #print("\r%d species and %d proteins loaded." % (len(self.__species), len(self.__proteins)), end="")
                                print("\r%s\r%s data loaded." % (" " * 80, humanize.naturalsize(f.tell())), end="")
                            """
                            protein = Protein(accession, sequence.replace("\n", ""), gene_id, gene_symbol, species)
                            self.__proteins.append(protein)
                            if species:
                                species.add(protein)
                        m = re.search("([^ ]*)([^\n]*)", f.readline())
                        accession = m.group(1)
                        sequence = ""
                        gene_id = ""
                        gene_symbol = ""
                        description = m.group(2)
                        m = re.search("\[([^\[\]]*)\]$", description)
                        if m:
                            scientific_name = m.group(1)
                        else:
                            scientific_name = ""
                        if f.tell() % 1024 == 0:
                            print("\r%s\r%s data loaded." % (" " * 80, humanize.naturalsize(f.tell()/1.024)), end="")
                    else:
                        sequence += c + f.readline()
                if scientific_name:
                    if scientific_name in species_dict:
                        species = species_dict[scientific_name]
                    else:
                        species = Species(scientific_name)
                        self.__species.append(species)
                        species_dict[scientific_name] = species
                else:
                    species = None
                """
                species = next((x for x in self.__species if x.scientific_name == scientific_name), None)
                if not species and scientific_name:
                    species = Species(scientific_name)
                    self.__species.append(species)
                """
                protein = Protein(accession, sequence.replace("\n", ""), gene_id, gene_symbol, species)
                self.__proteins.append(protein)
                if species:
                    species.add(protein)
                print("\r%s\r%s data loaded." % (" " * 80, humanize.naturalsize(f.tell())))
                print("%d species and %d proteins loaded." % (len(self.__species), len(self.__proteins)))
            self.__loaded = True
        else:
            raise RuntimeError("Already loaded.")
    def digest_single_threaded(self):
        if self.__loaded and not self.__digested:
            for i, species in enumerate(self.__species):
                species.digest()
                print("\r%d species proteins digested." % i, end="")
            print()
            self.__digested = True
        else:
            raise RuntimeError("Already digested or not loaded.")
    def digest(self):
        if self.__loaded and not self.__digested:
            counter = Value("i", 0)
            #species = list(self.__species)
            pool = Pool(initializer=init_digest, initargs=(counter, ))
            self.__species = pool.map(digest_worker, self.__species)

            """
            longest_peptide_length = Value("i", 0)
            species = list(self.__species)
            pool = Pool(initializer=init_measure, initargs=(longest_peptide_length, ))
            pool.map(measure_worker, self.__species)
            self.__longest_peptide_length = longest_peptide_length.value
            """

            self.__digested = True
        else:
            raise RuntimeError("Already digested or not loaded.")
        print()
    def reduce_single_threaded(self):
        if self.__digested and not self.__reduced:
            for i, species_i in enumerate(self.__species):
                peptides_space = []
                for species_j in self.__species:
                    if species_j != species_i:
                        peptides_space.extend(species_j.peptides)
                species_i.unique_peptides = set(species_i.peptides) - set(peptides_space)
                print("\r%d species unique peptides identified." % i, end="")
            print()
            self.__reduced = True
        else:
            raise RuntimeError("Already reduced or not digested.")
    def reduce_pool(self):
        if self.__digested and not self.__reduced:
            species_list_list = []
            for species_i in self.__species:
                species_list = [species_i]
                for species_j in self.__species:
                    if species_j != species_i:
                        species_list.append(species_j)
                species_list_list.append(species_list)
            pool = Pool(processes=20)
            self.__species = pool.map(reduce_worker, species_list_list)
            self.__reduced = True
        else:
            raise RuntimeError("Already reduced or not digested.")
    def reduce(self):
        if self.__digested and not self.__reduced:
            if os.path.exists("cache.db"):
                os.remove("cache.db")
            conn = sqlite3.connect("cache.db")
            c = conn.cursor()
            c.execute("CREATE TABLE IF NOT EXISTS peptide (species TEXT, peptide TEXT)")
            c.execute("CREATE INDEX peptide_index ON peptide (peptide)")
            for i, species in enumerate(self.__species):
                values = []
                for j, peptide in enumerate(species.peptides):
                    if values and j % (SQLITE_MAX_VARIABLE_NUMBER // 2) == 0:
                        c.execute("INSERT INTO peptide (species, peptide) VALUES %s" % ", ".join(["(?, ?)" for t in range(len(values) // 2)]), tuple(values))
                        values = []
                    else:
                        values.extend([species.scientific_name, peptide])
                if values:
                    c.execute("INSERT INTO peptide (species, peptide) VALUES %s" % ", ".join(["(?, ?)" for t in range(len(values) // 2)]), tuple(values))
                print("\r%d species cached." % (i + 1), end="")
            print("\nRemoving duplicate peptides.")
            c.execute("CREATE TABLE unique_peptide AS SELECT species, peptide FROM peptide GROUP BY peptide HAVING COUNT(*) = 1")
            species = {}
            for s in self.__species:
                species[s.scientific_name] = s
            c.execute("SELECT species, peptide FROM unique_peptide")
            for i, row in enumerate(c):
                species[row[0]].add_unique_peptide(row[1])
                if i % 102400 == 0:
                    print("\r%d unique peptides assigned." % (i + 1), end="")
            print()
            c.close()
            conn.close()
            os.remove("cache.db")
            self.__reduced = True
        else:
            raise RuntimeError("Already reduced or not digested.")
    def export_unique_peptides(self):
        if self.__reduced:
            txt = "%s_unique_peptides.txt" %os.path.splitext(self.__path)[0]
            with open(txt, "w") as f:
                for species in self.__species:
                    f.write("%s\t%s" % (species, ";".join(list(species.unique_peptides))))
            print("%s was exported with unique peptides." % txt)
        else:
            raise RuntimeError("Not reduced yet.")
    @property
    def path(self):
        return self.__path
    @property
    def species(self):
        for species in self.__species:
            yield species
    @property
    def proteins(self):
        for protein in self.__proteins:
            yield protein

class Protein:
    def __init__(self, accession, sequence, gene_id, gene_symbol, species):
        self.__accession = accession
        self.__sequence = sequence
        self.__gene_id = gene_id
        self.__gene_symbol = gene_symbol
        self.__species = species
    def __str__(self):
        return self.__accession
    def __repr__(self):
        return "<Protein accession: %s>" % self.__accession
    @property
    def accession(self):
        return self.__accession
    @property
    def sequence(self):
        return self.__sequence
    @property
    def species(self):
        return self.__species

class Species:
    def __init__(self, scientific_name):
        self.__scientific_name = scientific_name
        self.__proteins = []
        self.__peptides = []
        self.__unique_peptides = []
        self.__digested = False
    def __str__(self):
        return self.__scientific_name
    def __repr__(self):
        return "<Species scientific name: %s>" % self.__scientific_name
    def add(self, protein):
        self.__proteins.append(protein)
    def add_unique_peptide(self, peptide):
        self.__unique_peptides.append(peptide)
    def digest(self):
        if not self.__digested:
            self.__peptides = list(set(list(itertools.chain.from_iterable([pyteomics.parser.cleave(sequence=protein.sequence, rule=pyteomics.parser.expasy_rules["trypsin"], missed_cleavages=2, min_length=6) for protein in self.__proteins]))))
            self.__digested = True
    @property
    def scientific_name(self):
        return self.__scientific_name
    @property
    def proteins(self):
        for protein in self.__proteins:
            yield protein
    @property
    def peptides(self):
        for peptide in self.__peptides:
            yield peptide
    @property
    def unique_peptides(self):
        for peptide in self.__unique_peptides:
            yield peptide
    @unique_peptides.setter
    def unique_peptides(self, value):
        self.__unique_peptides = value

def main():
    start = datetime.datetime.now()
    print("%s started." % start.strftime("%Y-%m-%d %H:%M:%S"))
    if len(sys.argv) < 2:
        #path = "/home/unixer/protozoa_20181126_protein_sample.faa"
        #path = "/home/unixer/protozoa_20181126_protein_big_sample.faa"
        path = "/home/unixer/archaea_20181126_protein.faa"
    else:
        path = sys.argv[1]
    fasta = Fasta(path)
    fasta.load()
    fasta.digest()
    fasta.reduce()
    fasta.export_unique_peptides()
    finish = datetime.datetime.now()
    print("%s finished." % finish.strftime("%Y-%m-%d %H:%M:%S"))
    print("%.2f seconds spent." % (finish - start).total_seconds())

if __name__ == "__main__":
    main()

#path = "/home/unixer/protozoa_20181126_protein_big_sample.faa"
#path = "/home/unixer/bacteria_20181126_protein.faa"
#path = "/home/unixer/archaea_20181126_protein.faa"
#path = "/home/unixer/protozoa_20181126_protein.faa"
#path = "/home/unixer/protozoa_20181126_protein_big_sample.faa"
