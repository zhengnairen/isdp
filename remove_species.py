import humanize
with open("archaea_bacteria_fungi_protozoa_human_unique_peptides.fasta", "r") as fi:
    with open("a_b_f_p_h_one.fasta", "w") as fo:
        i = 0
        while True:
            line = fi.readline()
            i += 1
            if line:
                if line.startswith(">"):
                    if i == 1:
                        fo.write(line)
                else:
                    fo.write(line)
                if i % 10240 == 0:
                    print("\r%s\r%s data loaded." % (" " * 80, humanize.naturalsize(fi.tell())), end="")
            else:
                break
