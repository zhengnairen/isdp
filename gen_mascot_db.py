import humanize
with open("/home/power/projects/trypsin/unique_pep.fasta", "r") as fi:
    with open("/home/unixer/projects/isdp/unique_pep_for_mascot.fasta", "w") as fo:
        i = 1
        line = fi.readline()
        while True:
            if line:
                sequence, species = tuple(line.strip().split("\t"))
                fo.write(">%d|%s\n%s\n" % (i, species, sequence))
                i += 1
                line = fi.readline()
                if fi.tell() % 10240 == 0:
                    print("\r%s\r%s data processed." % (" " * 80, humanize.naturalsize(fi.tell())), end="")
                    #print("\033[F%s data processed." % humanize.naturalsize(fi.tell()))
            else:
                break
